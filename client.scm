(use-modules (ice-9 rdelim))
(use-modules (srfi srfi-1))
(use-modules (packedobjects packedobjects))
(use-modules (ice-9 pretty-print))
(use-modules (ice-9 match))
(load "net.scm")
(define IP "127.0.0.1")
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Response section.
;; This section contains responses to display.

;; big-reponse is the integration of seperate responses.
;; In this case it contains response-laptop and response-no.
(define (big-response)
  (append
   response-laptop 
   response-no))  
(define response-laptop '[(1 "Hello my friend, are you interested in buying laptop?") 
			  (2 "Ok, what on kind of laptop are you looking for?")
			  (3 "I am assuming that you would like to see information on the low-priced laptops.")
			  (4 "I am assuming that you would like to see information on the average-priced laptops.")
			  (5 "I am assuming that you would like to see information on the high-priced laptops")
			  (6 "Alright then, please choose from one of the following options:\n *Option1 - Cheap priced\n *Optiop2 - Average priced\n *Option3 - Expensive priced")
			  (7 "Would you like to see more information from the other price options?")])

(define response-no '[(10 "Many laptops on sale, still not interested?")
		      (11 "You are leaving the Intelligent Laptop Selling Bot(ILSB), are you sure?")])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Keyword section.
;; This section contains keywords in order to match with the user input.

;; big-keyword is the integration of seperate keywords.
;; In this case it contains key-laptop and key-no. 
(define (big-keyword)
  (append
   key-laptop
   key-no))
(define key-laptop '[(1 (2 (laptop)) (2 (laptops)) (2 (latpop)) (2 (laptpo)) (2 (lptop)) (2 (ltapop)) (2 (lappot)) (2 (lapot)) (2 (laptp))  (2 (buy)) (2 (looking)) (2 (see)) (2 (interested))
			(2 (yes)) (2 (y)) (2 (yees)) (2 (yeees)) (2 (right)) (2 (interested)) (2 (buy)) (2 (buying)) (2 (show)) (2 (searching))
			(3 (cheap laptop)) (3 (cheap laptops)) (3 (acer)) (3 (asus))
			(4 (average priced laptop)) (4 (average priced laptops)) (4 (sony)) (5 (dell)) (5 (lenova)) (5 (samsung))
			(5 (expensive laptop)) (5 (expensive laptops)) (5 (alienware)) (5 (mac)) (5 (macbook)) (5 (apple)) 	    
			(10 (no)) (10 (n))  (10 (noo)) (10 (nooo)) (10 (not interested))
			(0 (quit))) 
		     (2 (3 (cheap)) (3 (low price)) (3 (present)) (3 (cheapest)) (3 (value)) (3 (slow)) 
			(4 (reasonable)) (4 (middle)) (4 (good))(4 (average)) (4 (study)) (4 (studying)) (4 (school)) (4 (uni)) (4 (university)) (4 (anything)) 
			(5 (expensive)) (5 (gaming)) (5 (fast)) (5 (high performance)) (5 (best)) (5 (server)) (5 (play)) (5 (games)) (5 (business)) (5 (job)) (5 (work)) (5 (nice))
			(1 (go back))
			(0 (quit)))
		     (3 (cheap (yes)) (cheap (y)) 
			(6 (no)) (6 (wrong)) (6 (not))
			(2 (go back))
			(0 (quit)))
		     (4	(average (yes)) (average (y)) 
			(6 (no)) (6 (wrong)) 
			(2 (go back))
			(0 (quit)))
		     (5 (expensive (yes)) (expensive (y)) 
			(6 (no)) (6 (wrong)) 
			(2 (go back))
			(0 (quit)))
		     (6 (cheap (option1)) (3 (cheap)) (3 (#{1}#)) 
			(average (option2)) (4 (average)) (4 (#{2}#)) 
			(expensive (option3)) (5 (expensive)) (5 (#{3}#))
			(10 (no))
			(2 (go back))
			(0 (quit)))
		     (7 (6 (yes))
			(0 (no))
			(6 (go back))
			(0 (quit)))])

(define key-no '[(10 (2 (interested)) (2 (buy)) (2 (show))
		     (11 (no)) (11 (yes)) (11 (alright))
		     (2 (go back))
		     (0 (quit)))
		 (11 (0 (yes)) (0 (y)) 
		     (2 (no)) (2 (n)) 
		     (10 (go back))
		     (0 (quit)))])
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; The main program section. 
;; It can be start with different states.
(define (start state)  
  (for-each
   (lambda(actual-state) 
     (cond
      ;; This part displays the state responses.
      [(eq? (car actual-state) state) (format #t "~a\n" (cadr actual-state))])) (big-response))
  ;; Here I am defining input variable by reading user input as strings and converting it into symbols.
  (let [(input (map string->symbol (string-tokenize (string-downcase (read-line)))))]
    ;; Here I am finding the key by using find-key function and assigning it to key variable.
    (let ((key (find-key input state (reduce max 0 (list-of-lengths state)))))
      (cond
       ;; This part handles the unknown input.
       [(pair? key) (format #t "Unknown input please try again!\n") 
	(start (car key))]
       ;; If key is cheap or average or expensive then this part searches 
       ;; the related information from the database.
       [(or (eq? key 'cheap) (eq? key 'average) (eq? key 'expensive)) 
	(format #t "I found ~a" (count-library 'laptop (query key)))
	(format #t " laptops on the ~a"key)
	(format #t " price range.\n")
	(make-paragraph (query key) key)]
       ;; This part breaks the loop.
       [(eq? key 0) (format #t "Thanks for your time, see you again later!!!\n")]
       [(start key)]))))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sub functions for the main program.

;; This function finds key from the given list of state.
;; For example if the input contains laptop then it will return 2 etc.
(define (find-key input state maximum) 
  (cond
   [(eq? maximum 0) (cons state '(a))]
   [(eq? (compare-keys input state maximum) 'nothing) (find-key input state (- maximum 1))]
   [(compare-keys input state maximum)]))

;; Find-key function uses this sub function to do the matching process. 
(define (compare-keys input state maximum)
  (let loop2 ((state-list (assq-ref (big-keyword) state)))
    (let loop1 ((v-input input))
      (cond
       [(null? state-list) 'nothing]
       [(null? v-input) (loop2 (cdr state-list))]       
       [(and (>= (length v-input) maximum)
	     (eq? (match-max (take v-input maximum) (cadar state-list)) maximum)) (caar state-list)]
       [(loop1 (cdr v-input))]))))

;; Compare-keys uses this sub function to do the actual matching between the keywords.
(define (match-max input state-list)
  (cond
   [(or (null? input) (null? state-list)) 0]
   [(and (eq? (length input) (length state-list)) 
	 (eq? (car input) (car state-list))) (+ 1 (match-max (cdr input) (cdr state-list)))]
   [(match-max (cdr input) (cdr state-list))]))

;; The main program uses this sub function to find the maximum length from the specific state.
(define (list-of-lengths state)
  (let loop ((state-list (assq-ref (big-keyword) state)))
    (cond
     [(null? state-list) '()]
     [(cons (length (cadr (car state-list))) (loop (cdr state-list)))])))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sub functions to display the output as a paragraph.

;; This is a recursive function to display output as sentences using get-attribute function.
(define (make-paragraph output key)
  ;; output is simply just output from the search.
  ;; v-output same as output and it is used for recursive loop.
  (let loop [(v-output output)
	     (count 1)]
    (cond
     [(null? v-output) (start 7)]
     [(list? v-output) 
      (format #t " The ~a"count) 
      (format #t " laptop I have found is ~a" (get-attribute v-output 'name '0))  
      (format #t ". The laptop is powered with ~a" (get-attribute v-output 'cpu 'title))
      (format #t " CPU with speed of ~a" (get-attribute v-output 'cpu 'frequency))
      (format #t " frequency, cache is ~a" (get-attribute v-output 'cpu 'cache))
      (format #t ", and the CPU has ~a" (get-attribute v-output 'cpu 'cores))
      (format #t " cores. Also the RAM size for this specific model is ~a" (get-attribute v-output 'memory '0))
      (format #t " and the screen size is ~a" (get-attribute v-output 'screen '0))
      (format #t ". This laptop is a bargain within its ~a"key)
      (format #t " price range.")
      (format #t " Hurry now, it is on sale price costing just ~a" (get-attribute v-output 'price '0))
      (format #t "$ US dollars.\n")
      (loop (cdr v-output) (+ 1 count))])))

;; This function gets attributes from the search output,
;; for exapmle if I input like (get-attribute output 'cpu 'frequency) then it finds frequency information from cpu
;; or if I input like (get-attribute output 'memory '0) then it just finds the memory information from the database.
(define (get-attribute output attribute1 attribute2) 
  (let loop [(v-attribute1 attribute1)
	     (v-output (cdar output))]
    (cond
     [(null? v-output) "Unknown"] ;; If attribute not found then display unknown.
     [(eq? v-attribute1 (caar v-output))
      ;; If information found in attribute1 is pair then do recursive with attribute2. 
      (if (pair? (cadar v-output)) (loop attribute2 (cdar v-output))
	  ;; If the attribute is symbol then convert it to string.
	  ;; I am converting symbols to string because some enumerated datas (e.g #{1MB}#).
	  (if (symbol? (cadar v-output)) (symbol->string (cadar v-output))
	      ;; If the attribute is not symbol then just display the output.
	      (cadar v-output)))]
     [(loop v-attribute1 (cdr v-output))])))

;; This function counts the given title in the library of the database.
(define (count-library title output)
  (let loop ((v-output output))
    (cond
     [(null? v-output) 0]
     [(eq? (caar v-output) title) (+ 1 (loop (cdr v-output)))]
     [(loop (cdr v-output))])))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
;; Client side functions for receiving information from the server.
(define (query b)
  (let* [(pricetag (symbol->string b))
	 (sock (car conn))
	 (addr (cadr conn))]
    ;; send encoded messsage across network					
    (sendmessage sock addr (encode protocol (make-query pricetag)))
    ;; match the data we need	      	      
    (client-match-response
     ;; get message from network and decode it
     (let* ((message (readmessage sock))
	    (pdu (cdr message)))
       (decode protocol pdu)))))

(define (make-query pricetag)
  `(message
    (query
     (pricetag ,pricetag))))

(define (client-match-response result)
  (match result	
	 ;; did not find anything
	 [('message 
	   ('response 
	    ('library))) 
	  "Sorry no laptops found"]
	 ;; found some videos
	 [('message 
	   ('response 
	    ('library laptop ...)))
	  ;; return what we found
	  laptop]))

(define (make-connection)
  (let* ((sock (socket PF_INET SOCK_DGRAM 0))
	 (ipv4addr (inet-pton AF_INET IP))
	 (addr (make-socket-address AF_INET ipv4addr PORT)))
    (connect sock AF_INET ipv4addr PORT)
    `(,sock ,addr)))

;; make our network connection a global variable
(define conn (make-connection))

;; start the command loop
(start 1)




