(define protocol
  '(message choice
	    ;;client request
	    (query sequence
		   (pricetag string (size 1 100)))
	    ;;server response
	    (response sequence
		      (library sequence-of
			       (laptop sequence-optional
				       (pricetag string (size 1 100))
				       (name string (size 1 100))
				       (cpu sequence-of
				       	    (title string (size 1 100))
				       	    (frequency string (size 1 100))
				       	    (cache enumerated (1MB 2MB 3MB 4MB 5MB 6MB 7MB 8MB 15MB))
				       	    (cores enumerated (1 2 4 6 8 16)))			      
				       (memory enumerated (0.5GB 1GB 1.5GB 2GB 2.5GB 3GB 4GB 8GB 12GB 16GB 32GB))
				       (screen string (size 1 100))
				       (price integer (range 1 10000)))))))
