(use-modules (packedobjects packedobjects))
(use-modules (ice-9 match))
(load "net.scm")
(load "database.scm")

(define (server-get-query data)
  (match data
	 (('message
	   ('query
	    ('pricetag pricetag)))
	  ;; return
	  pricetag)))

(define (server-make-response keyword)
  `(message
    (response
     (library
      ,@(server-search keyword)))))

(define (server-search keyword)
  (filter
   (lambda (laptop)
     (match laptop
	    [('laptop ('pricetag pricetag) . rest)
	     (string-prefix-ci? keyword pricetag)]))
   (cdr database)))

(define (handle-query sock addr query)
  (define pricetag (server-get-query query))
  (format #t "search for :~a\n" pricetag)
  (sendmessage sock addr (encode protocol (server-make-response pricetag))))

(define (handle-error key . args)
  (format #t "error :~a ~a\n" key args))

(let ((sock (socket PF_INET SOCK_DGRAM 0)))
  (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
  (bind sock AF_INET INADDR_ANY PORT)
  (format #t "Listening for clients in pid:~S\n" (getpid))
  (while #t
	 (let* ((message (readmessage sock))
		(addr (car message))
		(pdu (cdr message))
		(query (decode protocol pdu)))
	   (catch #t
		  (lambda ()
		    (handle-query sock addr query)) handle-error))))


