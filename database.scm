(define database
  '(library
    (laptop
     (pricetag "Cheap")
     (name "Acer A1")       
     (cpu 
      (title "Celeron G440")
      (frequency "1GHz")
      (cache 1MB)
      (cores 1))
     (memory 0.5GB)
     (screen "11inch")
     (price 100)) 
    (laptop
     (pricetag "Cheap")
     (name "Lenova S30")
     (cpu 
      (title "Celeron G530")
      (frequency "1.6GHz")
      (cache 2MB)
      (cores 1))
     (memory 1GB)
     (screen "12inch")
     (price 200)) 
    (laptop
     (pricetag "Cheap")
     (name "IBM D70")
     (cpu 
      (title "Celeron G620")
      (frequency "1.8GHz")
      (cache 3MB)
      (cores 1))
     (memory 2GB)
     (screen "13inch")
     (price 250))
    (laptop
     (pricetag "Cheap")
     (name "Dell Lat80")
     (cpu 
      (title "Celeron G840")
      (frequency "2GHz")
      (cache 3MB)
      (cores 1))
     (memory 1.5GB)
     (screen "13inch")
     (price 300))
    (laptop
     (pricetag "Average")
     (name "Dell Insp14")
     (cpu 
      (title "Intel-Core2")
      (frequency "2.2GHz")
      (cache 3MB)
      (cores 2))
     (memory 2GB)
     (screen "14inch")
     (price 350))
    (laptop
     (pricetag "Average")
     (name "SONY VAIO15")
     (cpu 
      (title "Intel-core2")
      (frequency "2.4GHz")
      (cache 3MB)
      (cores 2))
     (memory 2.5GB)
     (screen "15inch")
     (price 400))
    (laptop
     (pricetag "Average")
     (name "SAMSUNG RV15")
     (cpu 
      (title "Intel-Core2x")
      (frequency "2.6GHz")
      (cache 4MB)
      (cores 2))
     (memory 3GB)
     (screen "15inch")
     (price 450))
    (laptop
     (pricetag "Expensive")
     (name "Macbook X17")
     (cpu 
      (title "Intel-Quad")
      (frequency "2.8GHz")
      (cache 5MB)
      (cores 4))
     (memory 12GB)
     (screen "17inch")
     (price 1500))
    (laptop
     (pricetag "Expensive")
     (name "Alienware M17X")
     (cpu 
      (title "Intel-Quad")
      (frequency "3.4GHz")
      (cache 8MB)
      (cores 4))
     (memory 12GB)
     (screen "17inch")
     (price 2000))
    (laptop
     (pricetag "Expensive")
     (name "Origin EO17x")  
     (cpu 
      (title "Intel-Xeon")
      (frequency "4GHz")
      (cache 15MB)
      (cores 6))
     (memory 32GB)
     (screen "22inch")
     (price 3000))))


     
 
     
